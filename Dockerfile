FROM python:3.10-alpine

WORKDIR /app

COPY ./app /app/

RUN pip install flask

CMD flask --app app:app run --host 0.0.0.0 --port 5000
